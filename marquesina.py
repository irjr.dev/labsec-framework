from colorama import Fore, Back, Style
from colorama import init

init() #para windows

def marca():
	print(Fore.RED + Style.BRIGHT +"********************************************************************************************")
	print(Fore.YELLOW + """
	    _         _    ___           ___                                  _   
	   | |   __ _| |__/ __| ___ __  | __| _ __ _ _ __  _____ __ _____ _ _| |__
	   | |__/ _` | '_ \__ \/ -_) _| | _| '_/ _` | '  \/ -_) V  V / _ \ '_| / /
	   |____\__,_|_.__/___/\___\__| |_||_| \__,_|_|_|_\___|\_/\_/\___/_| |_\_\\ 

		   Laboratorio de Redes - Universidad Nacional de TucumÃ¡n
	""")
	print(Fore.RED + "********************************************************************************************" + Style.NORMAL + Fore.RESET)
