# Labsec Framework

Labsec Framework es una herramienta de seguridad desarrollada como proyecto de graduación para mi titulación de Ingeniero en Computación por la Universidad Nacional de Tucumán (UNT). Esta herramienta de software permite realizar diferentes tipos de pruebas de seguridad, tanto en entornos IPv4 como en IPv6. Cuanta con las siguientes funcionalidades:

o	Realiza pruebas de Spoofing

o	Realiza pruebas de fuerza bruta a servicios como ftp, http y telnet.

o	Realiza escaneos de puertos.

o	Detecta dispositivos conectados en una red.

o	Realiza pruebas de denegación de servicios (DoS).

o	Realiza ataques de “Hombre en el Medio” (Man-in-the-Middle, MitM).

o	Capacidad para cargar módulos de forma dinámica en tiempo de ejecución.

o	Capacidad para que un usuario pueda crear módulos propios y extender las funcionalidades de la herramienta.


**Requisitos del sistema**

• El proceso de instalación requiere de conexión a internet, para poder descargar todas las librerías y dependencias necesarias.
• Sistema operativo GNU/Linux (basado en Debian preferentemente)
• Tener instalado Python 3.5 o superior.

**Instalación**

1. Copiar en el sistema el archivo labsec.zip y descomprimirlo.
2. Abrir una terminal y usando el comando cd ubicarse dentro del directorio del proyecto. Por ejemplo: cd /home/Ignacio/Documentos/labsec
3. Ejecutar la instrucción sudo python3 setup.py para iniciar la instalación.
4. Es posible que durante la instalación se le consulte si se desea instalar algunas dependencias necesarias. Se debe aceptar la instalación de todas las dependencias.


**Documentación**

Manual de usuario labsec: https://drive.google.com/open?id=162b-dlzPo8nXAzL7EI7O_4J0CRNXkQ0Y