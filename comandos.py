import importlib
from colorama import Fore, Back, Style, init
import marquesina
import platform
import os
import sys

 
if platform.system() == "Windows":
	init() #para colorear en  windows

def load(nombre_modulo):
	"""El comando 'load' permite cargar modulos de forma dinamica en tiempo de ejecucion.
	Los modulos se encuentran ubicados en la carpeta llamada 'modulos'
	ejemplo de uso: load nombre_del_modulo
	"""
	#nombre_modulo = "modulos/nombre_archivo"

	try:

		mod_namefile = nombre_modulo.split("/")
		comparador_archivo = (mod_namefile[0],mod_namefile[1])

		nombre_modulo = "modulos."+comparador_archivo[0]+"."+comparador_archivo[1]
		# ejemplo: #nombre_modulo = "modulos.scanner.ports"

		try:
			modulo = importlib.import_module(nombre_modulo)
			obtener = getattr(modulo, str(mod_namefile[1].capitalize()))()
			modulo = obtener
			orden = ""
			nombre_m = nombre_modulo.split('.')
			nombre_modulo = str(nombre_m[1]) +"/" +str(nombre_m[2])

			while orden != "back":

				if modulo:

						orden = input(Style.BRIGHT + "labsec("  + Fore.RED + nombre_modulo + Fore.RESET +")>> " + Style.NORMAL)
						comandos = orden.split(" ")
						if comandos[0] != "back":
							if comandos[0] == "clear":
								limpiar()
							
							elif comandos[0] == "exec":
								terminal(comandos)

							elif comandos[0] == "modules":
								modules()

							elif comandos[0] == "load":
								#del modulo
								try:
									load(comandos[1])
									#break
								except IndexError:
									print("El comando 'load' requiere que proporcione el nombre de un modulo: load <module>")

							elif comandos[0] == "exit":
								sys.exit()
							elif comandos[0] == "help":
								#Help("generico")
								try:
									if comandos[1].isspace() or comandos[1]=="":
										help_commnd("generico")
									else:
										help_commnd(comandos[1])
								except IndexError:
									help_commnd("generico")


							else:
								try: #comandos internos
									if comandos[0] == "run":
										#print("ddd", comandos[0])("")
										getattr(modulo, comandos[0])()
									elif comandos[0] == "options":
										if len(comandos)>1:
											getattr(modulo, comandos[0])(comandos[1:])
										else:
											getattr(modulo, comandos[0])("generic")

									else:
										getattr(modulo, comandos[0])(comandos[1:])
								except IndexError:
									print("El comando requiere de parametros. \nUtilice el comando {}{}help {}{}{} para obtener ayuda.".format(Style.BRIGHT ,Fore.CYAN, comandos[0], Style.NORMAL ,Fore.RESET))
								except Exception as e:
									if orden != "":
										print("El comando ingresado no es valido. \nUtilice el comando {}{}help{}{} para obtener ayuda.\n{}.".format(Style.BRIGHT ,Fore.CYAN, Style.NORMAL ,Fore.RESET, e))
									else:
										continue
			del modulo # libero memoria, eliminando el modulo luego de usarlo

		except ImportError:
			print("ERROR de importaciÃ³n!\nModulo no encontrado. Utilice el comando {}modules{} para listar los modulos disponibles.".format(Fore.YELLOW, Fore.RESET))
		
		except IndexError:
			print("ERROR IndexError!!\nModulo no encontrado. Utilice el comando {}modules{} para listar los modulos disponibles.".format(Fore.YELLOW, Fore.RESET))

		# except SyntaxError:
		# 	print("Error en el modulo: El archivo debe llamarse igual que la clase!")

		except Exception as e:
			print("Error: ", type(e), e)

	except IndexError:
		print("ERROR de importaciÃ³n!!!\nModulo no encontrado. Utilice el comando {}modules{} para listar los modulos disponibles.".format(Fore.YELLOW, Fore.RESET))

def Help(arg):
	if arg == "generico":
		print(Fore.YELLOW + "\nComandos globales:" + Fore.RESET)
		print("""
 help 				Imprime este menÃº de ayuda
 load <module>			Carga el mÃ³dulo seleccionado
 modules			Muestra una lista de todos los mÃ³dulos disponibles
 clear				Limpia el terminal
 exec <shell command> <args>	Ejecuta un comando shell
 exit 				Salir de labsec framework

Para mÃ¡s informaciÃ³n acerca de un comando escriba: {}help <nombre_del_comando> {}| ejemplo: {}help load{}
			""".format(Fore.YELLOW, Fore.RESET,Fore.YELLOW, Fore.RESET))
	elif arg == "__inter_":
		print("COMANDOS internos del")
	elif arg == "load":
		print("""El comando 'load' permite cargar mÃ³dulos de forma dinÃ¡mica en tiempo de ejecuciÃ³n. Recibe como parÃ¡metro el nombre del mÃ³dulo a cargar.

Sintaxis: {}load <modulo>
{}ejemplo de uso: {}load scanner/scan{}
	""".format(Fore.YELLOW,Fore.RESET,Fore.YELLOW, Fore.RESET))
	elif arg == "exit":
		print("El comando 'exit' permite terminar el programa y salir del framework")
	elif arg == "exec":
		print("""El comando 'exec' permite ejecutar comandos shell desde el intÃ©rprete.   

Sintaxis: {}exec <shell command> <args>
{}ejemplos de uso: {}exec ping 8.8.8.8
                 exec ifconfig
                 exec arp -a{}
	""".format(Fore.YELLOW,Fore.RESET,Fore.YELLOW, Fore.RESET))

	elif arg == "modules":
		print("El comando 'modules' muestra una lista de los mÃ³dulos disponibles.")
		#print("Los mÃ³dulos se ubican en la ruta: {}{}/modulos".format(Fore.YELLOW, os.getcwd()))

	elif arg == "clear":
		print("El comando 'clear' limpia el terminal.")

	elif arg == "help":
		Help("generico")

	else:
		print("No se encontrÃ³ informaciÃ³n para el comando '{}'".format(arg))


def help_commnd(arg):
	if arg == "generico":
		print(Fore.YELLOW + "\nComandos globales:" + Fore.RESET)
		print("""
 help 				Imprime este menÃº de ayuda
 load <module>			Carga el mÃ³dulo seleccionado
 modules			Muestra una lista de todos los mÃ³dulos disponibles
 clear				Limpia el terminal
 exec <shell command> <args>	Ejecuta un comando en la shell
 exit 				Salir de labsec framework""")

		print(Fore.YELLOW + "\nComandos del modulo:" + Fore.RESET)
		print("""
 set <option name> <value> 	Setea una opciÃ³n para el mÃ³dulo seleccionado
 options 			Imprime las opciones para un mÃ³dulo
 run 				Corre el mÃ³dulo seleccionado con los valores de opciones seteados
 back 				Salir del mÃ³dulo actual

Para mÃ¡s informaciÃ³n acerca de un comando escriba: {}help <nombre_del_comando> {}| ejemplo: {}help load{}
			""".format(Fore.YELLOW, Fore.RESET,Fore.YELLOW, Fore.RESET))

	elif arg == "load":
		print("""El comando 'load' permite cargar mÃ³dulos de forma dinÃ¡mica en tiempo de ejecuciÃ³n. Recibe como parÃ¡metro el nombre del mÃ³dulo a cargar.

Sintaxis: {}load <modulo>
{}ejemplo de uso: {}load scanner/scan{}
	""".format(Fore.YELLOW,Fore.RESET,Fore.YELLOW, Fore.RESET))
	elif arg == "exit":
		print("El comando 'exit' permite terminar el programa y salir del framework")
	elif arg == "exec":
		print("""El comando 'exec' permite ejecutar comandos shell desde el intÃ©rprete.

Sintaxis: {}exec <comando_shell> <args>
{}ejemplos de uso: {}exec ping 8.8.8.8
                 exec ifconfig
                 exec arp -a{}
	""".format(Fore.YELLOW,Fore.RESET,Fore.YELLOW, Fore.RESET))

	elif arg == "modules":
		print("El comando 'modules' muestra una lista de los mÃ³dulos disponibles.")
		#print("Los mÃ³dulos se ubican en la ruta: {}{}/modulos".format(Fore.YELLOW, os.getcwd()))

	elif arg == "set":
		print("El comando 'set' permite setear los parÃ¡metros mostrados en options. Requiere como argumento el nombre de la opciÃ³n a setear y el valor asignado a esa opciÃ³n.")

	elif arg == "options":
		print("El comando 'options' lista los parÃ¡metros que se requieren configurar en el mÃ³dulo.")

	elif arg == "run":
		print("El comando 'run' jecuta el mÃ³dulo seleccionado y procesa las opciones seteadas.")

	elif arg == "back":
		print("El comando 'back' permite salir del mÃ³dulo actual.")

	elif arg == "help":
		Help("generico")

	else:
		print("No se encontrÃ³ informaciÃ³n para el comando '{}'".format(arg))


def modules():
	try:

		def lsdir(ruta = os.getcwd()):
			return [arch.name for arch in os.scandir(ruta) if arch.is_dir()]

		#modulos = []
		print()
		lsdir = lsdir("modulos")
		lsdir.sort()
		lsfile = []

		for direct in lsdir:
			if direct != "__init__" and direct[0]!="_":
				print(Fore.CYAN + Style.BRIGHT + " " + direct+"/" + Fore.RESET + Style.NORMAL)

				def lsfile(ruta = os.getcwd()):
					return [arch.name for arch in os.scandir(ruta) if arch.is_file()]

				lsfile = lsfile("modulos/" + direct)
				lsfile.sort()

				for lista in lsfile:
					mod = lista
					if mod != "__init__" and mod[0]!="_" and mod.find(".py") != -1:
						mod = lista[:len(lista)-3]
						print("    " + mod)
						#modulos.append(mod)

		print()

	except Exception as e:
		print("ERROR: ",e)


def terminal(args):
	try:
		cadena = ""
		args.pop(0)
		for i in args:
			cadena = cadena + str(i) + " "
		os.system(cadena)
	except IndexError:
		print("Error! en los parametros, 'exec' requiere que proporcione un comando shell.")
	except Exception as e:
		print("ERROR: ",e)


def limpiar():
	if platform.system() == "Windows":
		os.system("cls")
	else:
		os.system("clear")
	marquesina.marca()
