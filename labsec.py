from colorama import Fore, Back, Style, init
import comandos
import marquesina
import sys
import os
import readline

os.system("clear")
	
marquesina.marca()

def main():
	while True:

		try:

			comando = input(Fore.RESET + Style.BRIGHT + "labsec"+ Fore.RED + ">> " + Fore.RESET + Style.NORMAL)

			l_comando = list(comando.split(" "))

			if l_comando[0] == "load":
				try:
					comandos.load(l_comando[1])
					l_comando[0]= "" # sirve solamente para este while
				except IndexError:
					print("El comando 'load' requiere que proporcione el nombre de un modulo: load <module>")
				except Exception as er:
					print("ERROR: ", er)
			
			elif l_comando[0] == "help":
				try:
					if l_comando[1].isspace() or l_comando[1]=="":
						comandos.Help("generico")
					else:
						comandos.Help(l_comando[1])
				except IndexError:
					comandos.Help("generico")

			elif l_comando[0] == "clear":
				comandos.limpiar()

			elif l_comando[0] == "exec":
				comandos.terminal(l_comando)
				
			elif l_comando[0] == "modules":
				comandos.modules()
					
			elif l_comando[0] == "exit":
				sys.exit()
			elif l_comando[0] == "":
				continue
			else:
				print("Comando no valido!. Use el comando {}help{} para obtener mas informaciÃ³n".format(Fore.YELLOW, Fore.RESET))

		except KeyboardInterrupt: #si el usuario interrumpe desde teclado, vuelve al prompt principal
			print("")
		except Exception as e:
			print("ERROR critico: ", e)

if __name__ == "__main__":
	main()
