import sys
import os
import time

def version():
	try:
		version = sys.version
		if version.find("3.5")!= -1 or version.find("3.6")!= -1 or version.find("3.7")!= -1 or version.find("3.8")!= -1 or version.find("3.9")!= -1:
			print("Version de python3 [OK]")
		else:
			print("Labsec requiere Python 3.5 o superior. Por favor actualice la versión.")
			sys.exit(0)

	except Exception as e:
		print("Labsec requiere Python 3.5 o superior. Por favor actualice su versión.")
		print(e)

def configuracion():
	try:
		ruta = os.getcwd()
		#print(ruta)

		ruta = os.getcwd()
		rutaarchivo = ruta + '/extras/labsec'
		archivo = open(rutaarchivo , 'w')
		codigoCliente = '''
		echo labsec requiere permiso de administrador...
		cd {}
		sudo python3 labsec.py
		clear
		'''.format(ruta)
		archivo.write(codigoCliente)
		archivo.close()
		os.system("sudo chmod 777 " + rutaarchivo)

		os.system("cp extras/labsec /usr/local/bin")
		os.system("sudo chmod 777 /usr/local/bin/labsec")
	except Exception as e:
		print("Error al intentar configurar Labsec.")
		print(e)

def dependencias():
	try:
		os.system("sudo apt-get install python3-setuptools")
		os.system("sudo apt-get -y install python3-pip")
		os.system("sudo apt-get install zenmap")
		os.system("sudo apt-get install netcat")
		os.system("sudo apt-get install xterm")
		os.system("sudo pip3 install scapy-python3==0.23")
		os.system("sudo pip3 install colorama")
		os.system("sudo pip3 install platform")
		os.system("sudo pip3 install python-nmap")
		os.system("sudo pip3 install dnspython3==1.14.0")

	except Exception as e:
		print("Error al intentar instalar dependencias.")
		print(e)

if __name__ == "__main__":
	version()
	configuracion()
	dependencias()