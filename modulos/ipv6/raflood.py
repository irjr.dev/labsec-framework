import subprocess
import os
import threading
from colorama import init, Fore, Back, Style
import time


class Raflood():
	iface = None

	def __info(self):
		print("""
OPTIONS INFO
El modulo no posee parametros de configuracion obligatorios.
""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 iface		{:<15}		Interfaz de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
			""".format(str(self.iface), Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: set <option name> <value>")

	def run(self):

		comando = 'python3 modulos/ipv6/_raflood.py ' + str(self.iface)
		def proceso():
			subprocess.call(['xterm', '-e', comando])
		hilo = threading.Thread(target=proceso)
		hilo.setDaemon(True)
		hilo.start()