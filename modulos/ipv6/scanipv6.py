import sys
from colorama import init, Fore, Back, Style
import time
import logging

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy.")
	sys.exit(0)

class Scanipv6():
	ip6global = ""
	iface = None

	#ip6global = "::"
	
	def __info(self):
		print("""
OPTIONS INFO
Solo debe configurar el nombre de la interfaz de su placa de red.
Y opcionalmente su IP global.
""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 ip6global	{:<15}		Su IPv6 global
 iface		{:<15}		Interfaz de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {} options info{}
			""".format(self.ip6global, str(self.iface), Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			elif args[0] == "ip6global":
				self.ip6global = args[1]
				print("[+] ip6global:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: <option name> <value>")

	def run(self):
		conf.verb=0
		try:

			eth = Ether(dst="33:33:00:00:00:01")
			if self.ip6global == "":
				self.ip6global= "::"
			ipv6g = IPv6(src=self.ip6global, dst = 'ff02::1')
			ipv6 = IPv6(dst = 'ff02::1')
			icmpv6 = ICMPv6EchoRequest()
			paquete = eth/ipv6 / icmpv6
			paqueteg = eth/ipv6g / icmpv6

			ip_v6= []
			print("\nEscaneando red local...\n")

			resp_exitosa, resp_falla = srp(paquete, timeout = 1.5, multi = True, iface=self.iface)
			for (solicitud, respuesta) in resp_exitosa:
				ip_v6.append((respuesta[IPv6].src, respuesta.src))

			resp_exitosa2, resp_falla = srp(paqueteg, timeout = 1.5, multi = True, iface=self.iface)
			for (solicitud, respuesta) in resp_exitosa2:
				ip_v6.append((respuesta[IPv6].src, respuesta.src))

			ips= set(ip_v6)
			ips = list(ips)

			print("-------------------------------------------------------")
			print(Fore.RED + Style.BRIGHT  + "   	IPv6 Address                 MAC Address              " + Back.RESET + Fore.RESET + Style.NORMAL)
			print("-------------------------------------------------------")

			for ip_ in ips:
				print(" {:<33}{:<17}".format(ip_[0], ip_[1]))
			print()
		except Exception as e:
			print("Error: la ip global ingresada no es vÃ¡lida.")
			print(e)
