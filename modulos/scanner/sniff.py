from colorama import init, Fore, Back, Style
import threading
import subprocess



class Sniff():
	target1 = ""
	target2 = ""
	iface = None

	
	def __info(self):
		print("""
OPTIONS INFO

[+] target1: DirecciÃ³n ip del objetivo1.

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target1 192.168.1.105		 DirecciÃ³n ip del objetivo1

[+] target2: DirecciÃ³n ip del objetivo2.

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target2 192.168.1.108		 DirecciÃ³n ip del objetivo2

""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 target1 	{:<15}		DirecciÃ³n ip del objetivo1
 target2 	{:<15}		DirecciÃ³n ip del objetivo2
 iface		{:<15}		Interfaz de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
			""".format(self.target1, self.target2, str(self.iface), Fore.YELLOW, Fore.RESET))


	def set(self,args):
		try:
			if args[0] == "target1":
				self.target1 = args[1]
				print("[+] target1:",args[1])
			elif args[0] == "target2":
				self.target2 = args[1]
				print("[+] target2:",args[1])
			elif args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: set <option name> <value>")

	def run(self):
		comando = 'python3 modulos/scanner/_sniff.py ' + self.target1 + ' ' + self.target2 + ' ' + str(self.iface)
		def proceso():
			subprocess.run(['xterm','-e', comando])
		hilo = threading.Thread(target=proceso)
		hilo.setDaemon(True)
		hilo.start()

