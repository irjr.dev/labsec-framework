import sys
from colorama import init, Fore, Back, Style
import time
import logging
import socket
import struct


logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy.")
	sys.exit(0)

class Liveshosts():
	target = ""
	method = "arp"
	iface = None
	
	def __info(self):
		print("""
OPTIONS INFO

[+] target: DirecciÃ³n ip de la red a escanear.

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target 192.168.1.0/24		 direcciÃ³n de red a escanear


[+] method: Metodo usado en el escaneo <tcpsyn, icmp, arp>

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set method tcpsyn			 Usa mensajes TCP-SYN para realizar el escaneo 
 					 de hosts activos
 [-] set method arp			 Usa mensajes ARP request y ARP Reply para realizar 
 					 el escaneo de hosts activos
 [-] set method icmp			 Usa mensajes ICMP echo-request y echo-reply para realizar 
 					 el escaneo de hosts activos
""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 target 	{:<15}		DirecciÃ³n ip de la red a escanear. ejemplo: 192.168.1.0/24
 method 	{:<15}		Metodo usado en el escaneo <tcpsyn, icmp, arp>
 iface		{:<15}		Interfaz de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
			""".format(self.target, self.method, str(self.iface), Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "target":
				self.target = args[1]
				print("[+] target:",args[1])
			elif args[0] == "method":
				self.method = args[1]
				print("[+] method:",args[1])
			elif args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("SEl comando debe tener la siguiente sintaxis: <option name> <value>")

	def run(self): #args es una lista de parametros
		def ip2int(addr):
			return struct.unpack("!I", socket.inet_aton(addr))[0]

		def int2ip(addr):
			return socket.inet_ntoa(struct.pack("!I", addr))

		mi_ip = IP(dst="")
		mi_mac = ""#Ether(dst="")

		if self.method == "arp":

			conf.verb=0
			net = self.target
			
			if "/" in net:

				iface= None

				if self.target != "":
					print("\nEscaneando hosts de la red {}{}[{}]{}{}\n".format(Fore.CYAN, Style.BRIGHT, net, Fore.RESET, Style.NORMAL))
					try:

						tinicial = time.time()
						vivos, muertos = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=net), iface=iface, retry=-2, timeout=2)
					
						# en la lista "vivos" se guardan las duplas solicitudes-respuestas a las peticiones que hacemos
						hosts_ips = []
						hosts_macs = []
						hostsN = []

						for solicitud, respuesta in vivos:
							# hosts_ips.append(respuesta[ARP].psrc)
							# hosts_macs.append(respuesta[ARP].hwsrc)
							mi_mac = respuesta[ARP].hwdst
							hostsN.append((respuesta[ARP].psrc, respuesta[ARP].hwsrc))
							hostsN.sort()

						# imprimimos por partalla la informacion antes del envenenamiento
						print("-------------------------------------------------------")
						print(Fore.RED + Style.BRIGHT  + "   IP Address                 MAC Address              " + Back.RESET + Fore.RESET + Style.NORMAL)
						print("-------------------------------------------------------")

						for i in range(0, len(hostsN)):
							print("  {:<15}          {}".format( hostsN[i][0],  hostsN[i][1]))
						print(" *{:<15}          {}".format(mi_ip.src, mi_mac))
						print()
						tfinal = time.time() 
						tiempo = tfinal - tinicial # Devuelve un objeto timedelta

						print("Tiempo de escaneo %.2f segundos. Se encontraron %d host activos\n" %(tiempo, len(hostsN) + 1))

					except Exception as e:
					 	print(e)
				else:
					print("ERROR! el numero de parametros es incorrecto.")
			else:
				print("ERROR! la direcciÃ³n de red debe seguir la siguiente sintaxis red/mask. Ejemplo 192.168.1.0/24")

		elif self.method == "tcpsyn":
			conf.verb=0
			mi_ip = IP(dst="")
			red = self.target

			if "/" in red:
				hostsp = self.target.split("/")
				host = hostsp[0]
				bitHosts = 32 - int(hostsp[1])
				cantidadHosts = 2 ** bitHosts

				print("\nEscaneando hosts de la red {}{}[{}]{}{}\n".format(Fore.CYAN, Style.BRIGHT, red, Fore.RESET, Style.NORMAL))
				tinicial = time.time()
				i=0
				c=0
				try:
					while i < cantidadHosts:
						ip_o = ip2int(host) # 167772161
						ip_ent = ip_o + i
						ip_dest = int2ip(ip_ent) # 192.168.1.100
						puertoOrigen = RandShort()
						if ip_dest == mi_ip.src:
							i=i+1
							continue
						else:
							paquete = IP(dst = ip_dest)/TCP(sport = puertoOrigen, dport = 80, flags = "S")
							respuesta = sr1(paquete, iface=self.iface, timeout = 0.06)

						if("NoneType" in str(type(respuesta))):
							print("{}{}  {:<17}     [NO]{}{}".format(Fore.RED, Style.BRIGHT, ip_dest, Fore.RESET, Style.NORMAL))
							i=i+1

						elif(respuesta.haslayer(TCP) and (respuesta.getlayer(TCP).flags == 20 or respuesta.getlayer(TCP).flags == 4 or respuesta.getlayer(TCP).flags == 18)):
							#print(ip_dest + "   [conectado]")
							p = IP(dst = host)/TCP(sport = puertoOrigen, dport = 80, flags = "R")
							rst = sr(p, iface=self.iface, timeout = 0.06)
							print("  {:<17}     [conectado]".format(ip_dest))
							i=i+1
							c=c+1
				except KeyboardInterrupt:
					print("")
				tfinal = time.time() 
				tiempo = tfinal - tinicial # Devuelve un objeto timedelta

				print("\nTiempo de escaneo %.2f segundos. Se encontraron %d host activos\n" %(tiempo, c))

			else:
				print("ERROR! la direcciÃ³n de red debe seguir la siguiente sintaxis red/mask. Ejemplo 192.168.1.0/24")



		elif self.method == "icmp":
			conf.verb=0
			mi_ip = IP(dst="")
			red = self.target
			hostsp = self.target.split("/")
			host = hostsp[0]
			bitHosts = 32 - int(hostsp[1])
			cantidadHosts = 2 ** bitHosts



			if "/" in red:

				print("\nEscaneando hosts de la red {}{}[{}]{}{}\n".format(Fore.CYAN, Style.BRIGHT, red, Fore.RESET, Style.NORMAL))
				tinicial = time.time()
				i=0
				c=0
				while i < cantidadHosts:
					ip_o = ip2int(host) # 167772161
					ip_ent = ip_o + i
					ip_dest = int2ip(ip_ent) # 192.168.1.100
					paquete = IP(dst = ip_dest)/ICMP(type=8)
					respuesta = sr1(paquete, iface=self.iface, timeout = 0.06)

					if("NoneType" in str(type(respuesta))):
						print("{}{}  {:<17}     [NO]{}{}".format(Fore.RED, Style.BRIGHT, ip_dest, Fore.RESET, Style.NORMAL))
						i=i+1

					else:
						#print(ip_dest + "   [conectado]")
						print("  {:<17}     [conectado]".format(ip_dest))
						i=i+1
						c=c+1

				tfinal = time.time() 
				tiempo = tfinal - tinicial # Devuelve un objeto timedelta

				print("\nTiempo de escaneo %.2f segundos. Se encontraron %d host activos\n" %(tiempo, c))


			else:
				print("ERROR! la direcciÃ³n de red debe seguir la siguiente sintaxis red/mask. Ejemplo 192.168.1.0/24")
		else:
			print("la opciÃ³n 'method' solo admite <tcpsyn>, <icmp> o <arp>")


