import dns.query
import dns
import dns.resolver
import dns.zone
import os
from colorama import init, Fore, Back, Style
import time


class Dnsaxfr():
	domain = ""
	
	def __info(self):
		print("""
OPTIONS INFO

[+] domain: Dominio a consultar

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set domain unt.edu.ar		 Dominio a consultar

""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 domain 	{:<24} Dominio a consultar

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
			""".format(self.domain, Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "domain":
				self.domain = args[1]
				print("[+] domain:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("Se esperaba set <option name> <value>")

	def run(self): #args es una lista de parametros
		try:
			l1= []
			l2=[]
			l3=[]
			servers= []
			servidores = dns.resolver.query(self.domain,'NS')
			l1 = servidores.response.to_text().split("\n")

			for item in l1:
				if item.find("IN NS")>0:
					l2.append(item)
			if len(l2) > 0:
				for i in l2:
					l3 =  tuple(i.split("IN NS"))
					if str(l3[1]).strip(".") != "":
						servers.append(str(l3[1]).strip("."))		

			print("\nSe encontraron {} servidores.\n".format(len(servers)))
			for sv1 in servers:
				print("Solicitando transferencia de zona al servidor", sv1)
				try:
					zona = dns.zone.from_xfr(dns.query.xfr(sv1.strip(" "), self.domain))
					print("Tranferencia EXITOSA, servidor", sv1.strip(" "),"\n")
					time.sleep(1)
					nombres = zona.nodes.keys()
					for n in nombres:
						print(zona[n].to_text(n))
					print("\n")
				except Exception as a:
					print("Transferencia fallida, servidor", sv1.strip(" "))
					print("Reply code: Not authoritative (9)\n")
		except Exception as e:
			print("ERROR: ",e)

