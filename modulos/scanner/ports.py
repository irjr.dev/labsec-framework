from colorama import init, Fore, Back, Style
import time
import nmap
import logging

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy.")
	sys.exit(0)

class Ports():

	target = ""
	ports = []
	iface = None
	
	def __info(self):
		print("""
OPTIONS INFO

[+] target: DirecciÃ³n ip de la red o host a escanear.

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target 192.168.1.1		 ip de host a escanear
 [-] set target 192.168.1.0/24		 direcciÃ³n de red a escanear

[+] iface: Interfaz de red de.

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set iface eth0			Interfaz de red del atacante

[+] ports: Se especifican los puertos a escanear. Por default
escanearÃ¡ los primeros 1000 puertos de servicios bien conocidos.

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set ports 21,22-25,80 		 escanea los puertos 21,22,23,24,25,80

""")



	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:

			if self.ports == [] or self.ports == ["default"] or self.ports == [""] or self.ports == ["",""]:
				self.ports = "default"
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 target 	{:<15}		DirecciÃ³n ip de la red o host a escanear. ejemplo: 192.168.1.1
 iface		{:<15}		Interfaz de red
 ports 		{:<20}	Puertos a escanear. Ejemplo: 80,433,444-689,789
 		{:<20}
 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
				""".format(self.target, str(self.iface), str(self.ports)[0:21], str(self.ports)[21:42],Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "target":
				self.target = args[1]
				print("[+] target:",args[1])
			elif args[0] == "ports":
				self.ports = args[1]
				print("[+] ports:",args[1])
			elif args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: set <option name> <value>")

	def run(self):
		listaPuertos=[]
		if self.ports == "default":
			listaPuertos = list(range(1,1001))

		else:
			puertos = self.ports.split(',')
			for p in puertos:
				if "-" in p:
					aux = p.split("-")
					listaaux = list(range(int(aux[0]), int(aux[1]) + 1))
					listaPuertos.extend(listaaux)
				else:
					listaPuertos.append(int(p))

		conf.verb = 0 #Que no muestre datos en pantalla
		#listaPuertos = list(range(70,100)) #La lista de puertos a escanear
		host2 = self.target #AquÃ­ la IP que quieres escÃ¡near


		nm = nmap.PortScanner()

		if "/" in host2:
			#print("\n")
			conectados = nm.scan(host2, arguments= '-sP') # ----------------->>>>
			ip_conectadas = list(conectados["scan"].keys())
			ip_conectadas.sort()
			mi_ip = IP(dst="")
			if mi_ip.src in ip_conectadas:
				ip_conectadas.remove(mi_ip.src)
			print("Hosts conectados", ip_conectadas)
		else:
			ip_conectadas = [host2]


		tinicial = time.time()
		for host in ip_conectadas:
			
			print("\nEscaneando puertos de {}{}[{}]{}{}".format(Fore.CYAN, Style.BRIGHT, host, Fore.RESET, Style.NORMAL))
			print("-"*55)
			print(Fore.RED + Style.BRIGHT + "{:>10}        {}               {}        ".format("PUERTO","ESTADO", "SERVICIO") + Back.RESET + Fore.RESET + Style.NORMAL)
			print("-"*55)
			for puerto in listaPuertos:
				puertoOrigen = RandShort()
				paquete = IP(dst = host)/TCP(sport = puertoOrigen, dport = puerto, flags = "S")
				respuesta = sr1(paquete, iface=self.iface, timeout = 0.06)
				if("NoneType" in str(type(respuesta))):
					pass
				elif(respuesta.haslayer(TCP) and respuesta.getlayer(TCP).flags == 0x12):
					p = IP(dst = host)/TCP(sport = puertoOrigen, dport = puerto, flags = "R")
					rst = sr(p, iface=self.iface, timeout = 0.06)
					try:
						servicio = socket.getservbyport(puerto)
					except:
						servicio = "desconocido"
					print("{:>7}/tcp        {:<13}        {}".format(puerto, "[ABIERTO]", servicio))
					#print("[ABIERTO]",puerto," ->",servicio)



		tfinal = time.time()
		tiempo = tfinal - tinicial # Devuelve un objeto timedelta
		print("\nTiempo de escaneo %.2f" %(tiempo), end=" segundos\n\n")