import sys
import logging

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy.")
	sys.exit(0)

target1 = sys.argv[1] #args[0]
target2 = sys.argv[2] #args[1]
if sys.argv[3] == 'None':
	iface = None
else:
	iface = sys.argv[3]
try:
	conf.verb=0
	print("Monitoreando paquetes entre {} y {}...\n".format(target1,target2))
	flagsD = {
	    1: 'FIN',
	    2: 'SYN',
	    4: 'RST',
	    8: 'PSH',
	    16: 'ACK',
	    32: 'URG',
	    64: 'ECE',
	    128: 'CWR',
	    24: 'PSH-ACK',
	    18: 'SYN-ACK',
	    17: 'FIN-ACK',
	    20: 'RST-ACK'
	}
	def escaner(pkt):

		if IP in pkt:
			ip_src= pkt[IP].src
			ip_dst= pkt[IP].dst

		if TCP in pkt:
			tcp_sport = pkt[TCP].sport
			tcp_dport = pkt[TCP].dport
			tcp_seq = pkt[TCP].seq
			tcp_ack = pkt[TCP].ack
			tcp_flags= pkt[TCP].flags

			print("IP(src:" + str(pkt[IP].src) + ", dst:" + str(pkt[IP].dst) + ")")
			print("TCP(sport:" + str(pkt[TCP].sport) + ", dport:" + str(pkt[TCP].dport) +", seq:"+ str(pkt[TCP].seq) + ", ack:"+ str(pkt[TCP].ack) +", flags: "+ flagsD[pkt[TCP].flags]+")")
			if pkt[TCP].flags == 24:
				print("PAYLOAD(", pkt.load.decode('UTF-8').strip(), ")\n")
			else:
				print("\n")

	a=sniff(filter="(src {} and dst {}) or (src {} and dst {})".format(target1,target2,target2,target1), iface=iface, prn= escaner)
	#a=sniff(filter="(src 192.168.0.103 and dst 192.168.0.101) or (src 192.168.0.101 and dst 192.168.0.103)", prn= lambda x:x.show())

	#python3 _sniff.py 192.168.0.101 192.168.0.106
except KeyboardInterrupt: #si el usuario interrumpe desde teclado, vuelve al prompt principal
	a=input("Sniffer detenido!")
except Exception as e:
	print("ERROR: ", e)
	a=input("Sniffer detenido!")