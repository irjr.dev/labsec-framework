from colorama import init, Fore, Back, Style
import threading
import subprocess
import os

class Floodicmp():
	target=""
	iface = None
	
	def __info(self):
		print("""
OPTIONS INFO
[+] target: DirecciÃ³n ip del host objetivo.

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target 192.168.1.105		 DirecciÃ³n ip del objetivo

""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 target		{:<15}		IP de la victima
 iface		{:<15}		Interface de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
			""".format(self.target, str(self.iface), Fore.YELLOW, Fore.RESET))


	def set(self,args):
		try:
			if args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			elif args[0] == "target":
				self.target = args[1]
				print("[+] target:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: set <option name> <value>")

	def run(self):
		try:

			comando = 'python3 modulos/dos/_floodicmp.py ' + self.target + ' ' + str(self.iface)
			def proceso():
				subprocess.run(['xterm', '-e', comando])
			hilo = threading.Thread(target=proceso)
			hilo.setDaemon(True)
			hilo.start()
		except Exception as e:
			print(e)
			input()
