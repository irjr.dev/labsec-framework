from colorama import init, Fore, Back, Style
import threading
import subprocess
import os

class Hjtcp():
	ipsrc = ""
	ipdst = ""
	dport= ""
	sport= ""
	seq = ""
	ack = ""
	iface = None
	
	def __info(self):
		print("""
OPTIONS INFO

[+] ipsrc: DirecciÃ³n ip del objetivo1.

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set ipsrc 192.168.1.105		 DirecciÃ³n ip del objetivo1 (ip suplantada)

[+] ipdst: DirecciÃ³n ip del objetivo2.

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set ipdst 192.168.1.108		 DirecciÃ³n ip del objetivo2

""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 ipsrc		{:<15}		DirecciÃ³n ip del objetivo1 (ip suplantada)
 ipdst		{:<15}		DirecciÃ³n ip del objetivo2
 sport		{:<15}		Puerto origen
 dport		{:<15}		Puerto destino
 seq		{:<15}		Numero de secuencia
 ack		{:<15}		Numero de acuse de recibo
 iface		{:<15}		Interfaz de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
			""".format(self.ipsrc, self.ipdst,self.sport, self.dport, self.seq, self.ack, str(self.iface), Fore.YELLOW, Fore.RESET))


	def set(self,args):
		try:
			if args[0] == "ipsrc":
				self.ipsrc = args[1]
				print("[+] ipsrc:",args[1])
			elif args[0] == "ipdst":
				self.ipdst = args[1]
				print("[+] ipdst:",args[1])
			elif args[0] == "sport":
				self.sport = args[1]
				print("[+] sport:",args[1])
			elif args[0] == "dport":
				self.dport = args[1]
				print("[+] dport:",args[1])
			elif args[0] == "seq":
				self.seq = args[1]
				print("[+] seq:",args[1])
			elif args[0] == "ack":
				self.ack = args[1]
				print("[+] ack:",args[1])
			elif args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: set <option name> <value>")

	def run(self):

		comando = 'python3 modulos/hijacking/_hjtcp.py ' + self.ipsrc + ' ' + self.ipdst +' ' + self.sport + ' ' + self.dport +' ' + self.seq + ' ' + self.ack + ' ' + str(self.iface)
		def proceso():
			subprocess.run(['xterm', '-e', comando])
		hilo = threading.Thread(target=proceso)
		hilo.setDaemon(True)
		hilo.start()