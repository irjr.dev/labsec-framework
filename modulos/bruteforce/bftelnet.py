import os
from colorama import init, Fore, Back, Style
import time
import threading
import subprocess

class Bftelnet():
	target = ""
	userslist = "users.txt"
	passwordlist = "pass.txt"
	
	def __info(self):
		print("""
OPTIONS INFO

[+] target: IP del equipo victima (daemon telnet)

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set target 192.168.0.105		 IP del equipo victima

[+] userslist: Diccionario de usuarios

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set userslist users.txt		 Diccionario de usuarios

[+] passwordlist: Diccionario de passwords

 Ejemplos de seteo			 Comentarios
 -----------------------------		-----------------------------
 [-] set passwordlist pass.txt		 Diccionario de passwords
""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 target 	{:<15}		IP del equipo victima (daemon telnet)
 userslist	{:<15}		Diccionario de usuarios
 passwordlist	{:<15}		Diccionario de passwords

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
			""".format(self.target,self.userslist, self.passwordlist, Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "target":
				self.target = args[1]
				print("[+] target:",args[1])
			elif args[0] == "userslist":
				self.userslist = args[1]
				print("[+] userslist:",args[1])
			elif args[0] == "passwordlist":
				self.passwordlist = args[1]
				print("[+] passwordlist:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: set <option name> <value>")

	def run(self): #args es una lista de parametros
		try:

			comando = 'python3 modulos/bruteforce/_bftelnet.py ' + self.target + ' ' + self.userslist + ' ' +  self.passwordlist
			def proceso():
				subprocess.call(['xterm', '-e', comando])
			hilo = threading.Thread(target=proceso)
			hilo.setDaemon(True)
			hilo.start()
		except Exception as e:
			print("ERROR!",e)
