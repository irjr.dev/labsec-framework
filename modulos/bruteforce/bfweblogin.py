import os
import threading
from colorama import init, Fore, Back, Style
import subprocess
import requests
import time
import sys

class Bfweblogin():

	target = ''
	varuser = ''
	varpass = ''
	user= ''
	wordlist = 'wordlist.txt'
	additionalp = ""
	flags = "statuscode:302"
	iface = None


	def __info(self):
		print("""
OPTIONS INFO

[+] target: direccion URL objetivo.

 Ejemplos de seteo			 	Comentarios
 -----------------------------		-----------------------------
 [-] set target www.login.com		 URL objetivo.


[+] varuser: variable de usuario

 Ejemplos de seteo			 	Comentarios
 -----------------------------		-----------------------------
 [-] set varuser usuario 		 Variable de usuario 


[+] varpass: variable de usuario

 Ejemplos de seteo			 	Comentarios
 -----------------------------		-----------------------------
 [-] set varpass password 		 Variable de contraseÃ±a


 [+] user: nombre de usuario a atacar (puede agregar un diccionario)

 Ejemplos de seteo			 	Comentarios
 -----------------------------		-----------------------------
 [-] set user admin 		 	Nombre de usuario 


 [+] wordlist: diccionario de passwords

 Ejemplos de seteo			 	Comentarios
 -----------------------------		-----------------------------
 [-] set wordlist wordlist.txt 		 Diccionario de passwords


 [+] additionalp: Parametros adicionales requeridos en el POST

 Ejemplos de seteo			 	Comentarios
 -----------------------------		-----------------------------
 [-] set additionalp parm1:valor1, parm2:valor2	 	Diccionario de passwords

 [+] flags: Campos del Header http a comparar

 Ejemplos de seteo			 	Comentarios
 -----------------------------		-----------------------------
 [-] set flags statuscode:302, Location:/index.htm	 	Campos del Header http a comparar (si se cumplen la password es la correcta)

""")

	def options(self, info=""):
		if info[0] == "info":
			self.__info()
		else:
			print("""
 Opciones del Modulo:

 Nombre		Valor 			DescripciÃ³n
 ------		-----			-----------
 target 	{:<15}		Direccion URL objetivo
 varuser	{:<15}		Variable de usuario
 varpass	{:<15}		Variable de password
 user	 	{:<15}		Nombre de usuario a atacar
 wordlist	{:<15}		Diccionario de passwords
 additionalp	{:<15}		Parametros adicionales requeridos en el POST
 flags		{:<15}		Campos del Header http a comparar
 iface		{:<15}		Interfaz de red

 Para mÃ¡s informaciÃ³n acerca de las opciones escriba el comando: {}options info{}
			""".format(self.target, self.varuser, self.varpass, self.user, self.wordlist, self.additionalp, self.flags, str(self.iface), Fore.YELLOW, Fore.RESET))

	def set(self,args):
		try:
			if args[0] == "target":
				self.target = args[1]
				print("[+] target:",args[1])
			elif args[0] == "varuser":
				self.varuser = args[1]
				print("[+] varuser:",args[1])
			elif args[0] == "varpass":
				self.varpass = args[1]
				print("[+] varpass:",args[1])
			elif args[0] == "user":
				self.user = args[1]
				print("[+] user:",args[1])
			elif args[0] == "wordlist":
				self.wordlist = args[1]
				print("[+] wordlist:",args[1])
			elif args[0] == "additionalp":
				self.additionalp = args[1]
				print("[+] additionalp:",args[1])
			elif args[0] == "flags":
				self.flags = args[1]
				print("[+] flags:",args[1])
			elif args[0] == "iface":
				self.iface = args[1]
				print("[+] iface:",args[1])
			else:
				print("'{}' no es una opciÃ³n valida".format(args[0]))
		except Exception as e:
			print("ERROR!",e)
			print("El comando debe tener la siguiente sintaxis: set <option name> <value>")

	def run(self):
		try:

			comando = 'python3 modulos/bruteforce/_bfweblogin.py ' + self.target + ' ' + self.varuser + ' ' +  self.varpass + ' ' + self.user + ' ' + self.wordlist + ' ' + self.additionalp + ' ' + self.flags + ' ' + str(self.iface)
			def proceso():
				subprocess.call(['xterm', '-e', comando])
			hilo = threading.Thread(target=proceso)
			hilo.setDaemon(True)
			hilo.start()
		except Exception as e:
			print("ERROR!",e)