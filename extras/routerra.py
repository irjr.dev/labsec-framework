# Este script debe ejecutarse en la pc que sera el router default para que envie mensajes RA periodicamente
import time
import os
import sys
from colorama import init, Fore, Back, Style
import time
import logging

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

try:
	from scapy.all import *
except:
	print("Necesitas tener scapy.")
	sys.exit(0)


defaultrouter = "fe80::29af:b6e2:572e:c1fa" ## link-local de la pc donde se ejecuta este script
iface = None

a = IPv6(src='fe80::29af:b6e2:572e:c1fa', dst = 'ff02::1')
b = ICMPv6ND_RA(routerlifetime=1200)
c = ICMPv6NDOptSrcLLAddr(lladdr='f8:1a:67:5b:aa:9e')
d = ICMPv6NDOptMTU()
e = ICMPv6NDOptPrefixInfo(prefixlen=64, prefix='2001:db8::')
os.system('echo 1 > /proc/sys/net/ipv6/conf/all/forwarding')
while True:
	send(a/b/c/d/e)
	time.sleep(5)